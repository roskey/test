from flask import Flask
from flask_restful import Api, Resource, reqparse
from flask_jwt import JWT
from Section5_backup.security import authenticate, identity, UserRegister
import sqlite3

connection = sqlite3.connect("data.db")
cursor = connection.cursor()
create_table = "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY , username text, password text)"
cursor.execute(create_table)
items_table = "CREATE TABLE IF NOT EXISTS items(name text, price float)"
cursor.execute(items_table)
connection.commit()
connection.close()

app = Flask(__name__)
app.secret_key = "test"
api = Api(app)
jwt = JWT(app, authenticate, identity)

items = []


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("price", type=float, required=True, help="This field cannot be empty")

    def get(self, name):
        info = next(filter(lambda i: i["name"] == name, items), None)
        item = {"name": info["name"], "price": info["price"]} if info else None
        return {"item": item}, 200 if item else 404

    def post(self, name):
        if next(filter(lambda i: i["name"] == name, items), None) is not None:
            return {"message": f"Item with name {name} already exists"}, 400
        new_item = {"name": name, "price": Item.parser.parse_args()["price"]}
        items.append(new_item)
        return {"item": new_item}, 201

    def put(self, name):
        item = next(filter(lambda i: i["name"] == name, items), None)
        if item is not None:
            item["price"] = Item.parser.parse_args()["price"]
        else:
            item = {"name": name, "price": Item.parser.parse_args()["price"]}
            items.append(item)
        return {"item": item}, 200

    def delete(self, name):
        global items
        items = list(filter(lambda i: i["name"] != name, items))
        return {"message": "Item deleted"}, 200


class Items(Resource):
    def get(self):
        return {"items": items}, 200


api.add_resource(Item, "/item/<string:name>")
api.add_resource(Items, "/items")
api.add_resource(UserRegister, "/register")
app.run(port=5000, debug=True)
