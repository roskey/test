from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required
from Section4.security import authenticate, identity

app = Flask(__name__)
app.secret_key = "jose"
api = Api(app)

jwt = JWT(app, authenticate, identity)  # /auth end point

items = {}


class Item(Resource):
    @jwt_required()
    def get(self, name):
        if name in items.keys():
            return items[name], 200
        return {"item": None}, 404

    def post(self, name):
        if name in items.keys():
            return {"message": "An item with name {} already exists".format(name)}, 400
        data = request.get_json()
        new_item = {"name": name, "price": data["price"]}
        items[name] = new_item
        return new_item, 201

    def put(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("price",
                            type=float,
                            required=True,
                            help="This field cannot be left blank")
        data = parser.parse_args()
        if name in items.keys():
            items[name]["price"] = data["price"]
            return items[name], 200
        new_item = {"name": name, "price": data["price"]}
        items[name] = new_item
        return new_item, 201

    def delete(self, name):
        if name in items.keys():
            items.pop(name)
            return {"message": "item deleted"}, 200
        return {"item": None}, 404


class ItemList(Resource):
    def get(self):
        return {"items": items}


api.add_resource(Item, "/item/<string:name>")  # the end point for item here
api.add_resource(ItemList, "/items")  # define the end point http://IP/items
app.run(port=5000, debug=True)
