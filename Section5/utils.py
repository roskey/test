from werkzeug.security import safe_str_cmp
import sqlite3
from flask_restful import Resource, reqparse


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("price", type=float, required=True, help="This field cannot be empty")
    search_command = "SELECT * FROM items WHERE  name=?"
    insert_command = "INSERT INTO items VALUES (?, ?)"
    delete_command = "DELETE FROM items WHERE name=?"
    update_command = "UPDATE items SET price=? WHERE name=?"

    @staticmethod
    def find_item_by_name(name):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        result = cursor.execute(Item.search_command, (name,))
        info = result.fetchone()
        connection.commit()
        connection.close()
        return {"name": info[0], "price": info[1]} if info else None

    @staticmethod
    def insert(item):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute(Item.insert_command, (item["name"], item["price"]))
        connection.commit()
        connection.close()

    @staticmethod
    def update(item):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute(Item.update_command, (item["price"], item["name"]))
        connection.commit()
        connection.close()

    def get(self, name):
        item = Item.find_item_by_name(name)
        return {"item": item}, 200 if item else 404

    def post(self, name):
        if self.find_item_by_name(name):
            return {"message": f"Item with name {name} already exists"}, 400
        item = {"name": name, "price": Item.parser.parse_args()["price"]}
        try:
            Item.insert(item)
        except:
            return {"message": "An error occurred inserting the item"}, 500  # internal server error
        return {"item": item}, 201

    def put(self, name):
        item = Item.find_item_by_name(name)
        price = Item.parser.parse_args()["price"]
        if item:
            try:
                item["price"] = price
                Item.update(item)
            except:
                return {"message": "An error occurred inserting the item"}, 500  # internal server error
        else:
            try:
                item = {"name": name, "price": price}
                Item.insert(item)
            except:
                return {"message": "An error occurred inserting the item"}, 500  # internal server error
        return {"item": item}, 200

    def delete(self, name):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute(Item.delete_command, (name,))
        connection.commit()
        connection.close()
        return {"message": "Item deleted"}, 200


class Items(Resource):
    get_all_command = "SELECT * FROM items"

    def get(self):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        result = cursor.execute(Items.get_all_command)
        items = []
        for row in result:
            item = {"name": row[0], "price": row[1]}
            items.append(item)
        connection.close()
        return {"items": items}, 200


class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("username", type=str, required=True, help="Please enter the username")
    parser.add_argument("password", type=str, required=True, help="Please enter the password")

    def post(self):
        data = UserRegister.parser.parse_args()
        if User.find_by_username(data["username"]):
            return {"message": f"A user with username {data['username']} already exist"}, 400
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        query = "INSERT INTO users VALUES (NULL, ?,?)"
        cursor.execute(query, (data["username"], data["password"]))
        connection.commit()
        connection.close()
        return {"message": "User created"}, 201


class User:
    def __init__(self, _id, username, password):
        self.id = _id
        self.username = username
        self.password = password

    @classmethod
    def find_by_username(cls, username):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE username=?"
        result = cursor.execute(query, (username,))
        row = result.fetchone()
        if row:
            user = cls(*row)
        else:
            user = None
        connection.close()
        return user

    @classmethod
    def find_by_id(cls, _id):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE id=?"
        result = cursor.execute(query, (_id,))
        row = result.fetchone()
        if row:
            user = cls(*row)
        else:
            user = None
        connection.close()
        return user


def authenticate(username, password):
    user = User.find_by_username(username)
    if user and safe_str_cmp(user.password, password):
        return user


def identity(payload):
    user_id = payload['identity']
    return User.find_by_id(user_id)


def create_table():
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()
    create_table = "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY , username text, password text)"
    cursor.execute(create_table)
    items_table = "CREATE TABLE IF NOT EXISTS items(name text, price float)"
    cursor.execute(items_table)
    connection.commit()
    connection.close()
