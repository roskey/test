from flask import Flask
from flask_restful import Api
from flask_jwt import JWT
from Section5.utils import authenticate, identity, UserRegister, Item, Items, create_table

app = Flask(__name__)
app.secret_key = "test"
create_table()
jwt = JWT(app, authenticate, identity)
api = Api(app)
api.add_resource(Item, "/item/<string:name>")
api.add_resource(Items, "/items")
api.add_resource(UserRegister, "/register")
if __name__ == '__main__':
    app.run(port=5000, debug=True)
